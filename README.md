# Descrição
Sistema simples de cadastro / login. Feito com PHP, PDO, JavaScript e Bootstrap.

# Como instalar
Na pasta principal do projeto, tem um backup do banco de dados (cadastro.sql), importe ele em seu banco de dados.
Em seguida abra o arquivo "connection.php", e configure com os dados do seu banco de dados. 
Feito isso, o sistema estará pronto para uso.
Você tambem pode ver ele online por esse link:
* <a href="http://registrationsystem.tk" target="_blank">registrationsystem.tk</a>

# Contribuição
Acha que algo poderia ser melhorado?
Envie um "Pull request" que em seguida faço uma analise, e se for uma melhoria considerável será aprovada!

# Recomendação
Adicionem um sistema de token no sistema de login e registro. Ainda no sistem de registro adicionem um Recaptcha. De preferencia o do Google!
