<?php 
	session_start();
	require_once('connection.php');
	require_once('functions.php');
	$functions = new Functions;
	$user = $_POST['user'];
	$password = md5($_POST['password']);
	try{
		$Search_User = $ConnPDO->prepare("SELECT * FROM cadastro WHERE user=:user AND password=:password;");
		$Search_User->bindValue(':user', strtolower($user), PDO::PARAM_STR);
		$Search_User->bindValue(':password', sha1($password." ".strtolower($_POST['user'])), PDO::PARAM_STR);
		$Search_User->execute();

		if ( $Search_User->rowCount() ) {
			//cria as sessões
			$_SESSION['user'] = $_POST['user'];
			$_SESSION['Tokken'] = md5($_POST['user']." ".$_POST['password']);
			$functions->{'AlertAndRedirect'}('Login Realizado Com Sucesso!','dashboard/');
		}else{
			$functions->{'AlertAndRedirect'}('Usuario ou senha invalidos', "//".$_SERVER['HTTP_HOST']);
		}
	}catch(PDOException $e) {
		echo $e->getCode()." ".$e->getMessage();
	}
?>