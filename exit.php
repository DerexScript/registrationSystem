<?php 
	session_start();
	require_once('functions.php');
	$functions = new Functions;
	if(isset($_SESSION['user']) && isset($_SESSION['Tokken'])){ 
		session_destroy();
		$functions->{'AlertAndRedirect'}('Logout efetuado com sucesso!', "//".$_SERVER['HTTP_HOST']);
	}else{
		$functions->{'AlertAndRedirect'}('Você não está logado!', "//".$_SERVER['HTTP_HOST']);
	}
?>